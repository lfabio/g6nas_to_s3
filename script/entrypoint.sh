#!/bin/bash

echo "Arguments: " $1 $2 $3 $4 $5

# if [[ -z "$NAS_USER" || -z "$NAS_PASS" ]]; then
#     echo "ERROR: USERNAME and PASSWORD of NAS must be defined" &> /dev/stderr
#     exit 1
# fi

if [[ -z "$2" ]]; then
    echo "USAGE: docker run <image> <NAS_PATH> <S3_PATH> [<FILE_TO_COPY>] [<NAS_INNER_PATH>] <DOMAIN>" &> /dev/stderr
    echo "EXAMPLE: docker run --env-file=prd.properties --env-file=prd.secrets -it iberia-dataprep/k8s-g6nas-to-s3:g6 //172.21.238.131/pvof/ s3://iberia-data-lake/revman/g6/raw/ G6_DETALLE_PETICIONES_DIARIO_200902_200916.csv Grupos/InformeG6/ GRUPOIBERIA" &> /dev/stderr
    exit 1
fi

NAS_PATH="$1"
S3_PATH="$2"
#MOUNTPOINT="."

if [[ -n "$5" ]]; then
    FILE_TO_COPY="$3"
    INNER_PATH="$4"
    DOMAIN="$5"
fi

# Mount NAS drive temporarily
# sudo mount -t cifs -o <Options> //<Server>/<Sharename> <Mountpoint> 
# -t     filesystem type (Common Internet File System, cifs)
# -vvv   verbose mode with increased verbosity
# -o     mount options, separated by commas
#   user        specifies the username to connect as.
#   domain      sets the domain (workgroup) of the user.
#   vers        version?
#   password    specifies the CIFS password.
#   sec         Security mode. (none: attempt to connection as a null user (no name)).
 
#sudo mount.cifs $NAS_PATH $MOUNTPOINT -o user=$USERNAME,dom=$DOMAIN,vers=1.0,pass=$PASSWORD,sec=none

echo "Listing Files" &> /dev/stderr

smbclient $NAS_PATH -U $NAS_USER%$NAS_PASS -W $DOMAIN -c "cd "$INNER_PATH"; ls"

smbclient $NAS_PATH -U $NAS_USER%$NAS_PASS -W $DOMAIN -c "cd "$INNER_PATH"; get "$FILE_TO_COPY""


if [[ "$?" != "0" ]]; then
    echo "File not found" &> /dev/stderr
    exit 0
fi

echo "File "$FILE_TO_COPY" copied" &> /dev/stderr

if [[ -n "$3" ]]; then
    aws s3 mv $FILE_TO_COPY $S3_PATH$FILE_TO_COPY || exit 1
else
    aws s3 mv $S3_PATH --recursive --exclude "*" --include "*.csv" || exit 1
fi
