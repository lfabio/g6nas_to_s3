#!/bin/bash

export ENV=$1
export VERSION=$2
export NAS_PATH=$3
export S3_PATH=$4
export FILE_TO_COPY="$5"
export INNER_PATH="$6"
export DOMAIN="$7"


function USAGE() {
  echo "USAGE: ./run-docker.sh <sbx|prd> <VERSION> <NAS_PATH> <S3_PATH> <FILE_TO_COPY> <INNER_PATH> <DOMAIN>" &>/dev/stderr
  exit 1
}

if [[ -z "$7" ]]; then
  USAGE
fi

if [[ "sbx" != "$ENV" && "prd" != "$ENV" ]]; then
  USAGE
fi

docker run \
  --env-file="$ENV".properties \
  --env-file="$ENV".secrets \
  -it 077156906314.dkr.ecr.eu-west-1.amazonaws.com/dataprep-k8s-g6nas-to-s3:${VERSION} $NAS_PATH $S3_PATH $FILE_TO_COPY $INNER_PATH $DOMAIN
