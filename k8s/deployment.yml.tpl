# Notice this YAML is templated (use envsubst to replace vars)
apiVersion: batch/v1
kind: Job
metadata:
  name: dataprep-k8s-g6nas-to-s3
spec:
  template:
    metadata:
      labels:
        app: dataprep-k8s-g6nas-to-s3
    spec:
      containers:
        - name: dataprep-k8s-g6nas-to-s3
          image: 077156906314.dkr.ecr.eu-west-1.amazonaws.com/dataprep-k8s-g6nas-to-s3:${VERSION}
          args: [
            ${NAS_PATH},
            ${S3_PATH},
            ${FILE_TO_COPY},
            ${NAS_INNER_PATH},
            ${DOMAIN}
          ]
          imagePullPolicy: "Always"
          env:
            - name: DOMAIN
              valueFrom:
                configMapKeyRef:
                  name: dataprep-k8s-g6nas-to-s3-configmap
                  key: DOMAIN
            - name: AWS_ACCESS_KEY_ID
              valueFrom:
                secretKeyRef:
                  name: dataprep-k8s-g6nas-to-s3-secrets
                  key: AWS_ACCESS_KEY_ID
            - name: NAS_USER
              valueFrom:
                secretKeyRef:
                  name: dataprep-k8s-g6nas-to-s3-secrets
                  key: NAS_USER
            - name: AWS_SECRET_ACCESS_KEY
              valueFrom:
                secretKeyRef:
                  name: dataprep-k8s-g6nas-to-s3-secrets
                  key: AWS_SECRET_ACCESS_KEY
            - name: NAS_PASS
              valueFrom:
                secretKeyRef:
                  name: dataprep-k8s-g6nas-to-s3-secrets
                  key: NAS_PASS
      # Do not restart containers after they exit
      restartPolicy: Never
