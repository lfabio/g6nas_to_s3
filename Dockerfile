# AUTHOR: NEXT DIGITAL HUB
# DESCRIPTION: A Docker image to execute BulkAPI Job's
# BUILD: docker build 

FROM ubuntu:20.04
LABEL maintainer="NEXT DIGITAL HUB"

env DEBIAN_FRONTEND=noninteractive

RUN apt-get update -y

# AWS CLI
RUN apt-get install -y -f curl unzip --option=Dpkg::Options::=--force-confdef
RUN curl "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip" -o "awscliv2.zip"
RUN unzip awscliv2.zip && ./aws/install

RUN apt-get install -y smbclient

COPY script/entrypoint.sh /entrypoint.sh
ENTRYPOINT ["/entrypoint.sh"]
