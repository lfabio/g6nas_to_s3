#!/bin/bash

# for envsubst
export ENV=$1
export VERSION=$2
export NAS_PATH=$3
export S3_PATH=$4
export FILE_TO_COPY="$5"
export INNER_PATH="$6"
export DOMAIN="$7"

declare -A EKS_CLUSTER
EKS_CLUSTER['sbx']='sbxeu-ibr-cluster-eks'
EKS_CLUSTER['prd']='prdeu-ibr-cluster-eks'

function USAGE() {
  echo "USAGE: ./run-k8s.sh <sbx|prd> <VERSION> <NAS_PATH> <S3_PATH>" &>/dev/stderr
  exit 1
}

if [[ -z "$4" ]]; then
  USAGE
fi

if [[ "sbx" != "$ENV" && "prd" != "$ENV" ]]; then
  USAGE
fi

if [[ -z "$AWS_PROFILE" ]]; then
  echo "ERROR: AWS_PROFILE environment variable must be defined" &> /dev/stderr
  exit 1
fi

# EKS login
aws --profile="$AWS_PROFILE" eks --region eu-west-1 update-kubeconfig --name "${EKS_CLUSTER[$ENV]}"

# Re-create configmaps
kubectl --cluster=arn:aws:eks:eu-west-1:077156906314:cluster/${EKS_CLUSTER[$ENV]} delete configmap dataprep-k8s-g6nas-to-s3-configmap
kubectl --cluster=arn:aws:eks:eu-west-1:077156906314:cluster/${EKS_CLUSTER[$ENV]} create configmap dataprep-k8s-g6nas-to-s3-configmap --from-env-file=${ENV}.properties 

# Re-create secrets
kubectl --cluster=arn:aws:eks:eu-west-1:077156906314:cluster/${EKS_CLUSTER[$ENV]} delete secret dataprep-k8s-g6nas-to-s3-secrets
kubectl --cluster=arn:aws:eks:eu-west-1:077156906314:cluster/${EKS_CLUSTER[$ENV]} create secret generic dataprep-k8s-g6nas-to-s3-secrets --from-env-file="${ENV}".secrets

# Apply deployment (substituting shell variables with envsubst)
kubectl --cluster=arn:aws:eks:eu-west-1:077156906314:cluster/${EKS_CLUSTER[$ENV]} delete job dataprep-k8s-g6nas-to-s3
envsubst < deployment.yml.tpl | kubectl --cluster=arn:aws:eks:eu-west-1:077156906314:cluster/${EKS_CLUSTER[$ENV]} apply -f -
