#!/bin/bash

if [[ -z "$1" ]]; then
  echo "USAGE: ./build.sh <VERSION>" &> /dev/stderr
  exit 1
fi

if [[ -z "$AWS_PROFILE" ]]; then
  echo "ERROR: AWS_PROFILE environment variable must be defined" &> /dev/stderr
  echo "To define AWS_PROFILE execute this command: export AWS_PROFILE=<your_profile_from_~/.aws/credentials>" &> /dev/stderr
  exit 1
fi

VERSION="$1"

docker build --rm -t iberia-dataprep/k8s-g6nas-to-s3:"${VERSION}" .

docker tag iberia-dataprep/k8s-g6nas-to-s3:"${VERSION}" 077156906314.dkr.ecr.eu-west-1.amazonaws.com/dataprep-k8s-g6nas-to-s3:"${VERSION}"

#$(aws --profile="$AWS_PROFILE" ecr get-login --no-include-email)

aws --profile="$AWS_PROFILE" ecr get-login-password \
    --region eu-west-1 \
| docker login \
    --username AWS \
    --password-stdin 077156906314.dkr.ecr.eu-west-1.amazonaws.com

if [[ "$?" != "0" ]]; then
  exit 1
fi

docker push 077156906314.dkr.ecr.eu-west-1.amazonaws.com/dataprep-k8s-g6nas-to-s3:"$VERSION"
